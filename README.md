<img src="themes/jnktn-main/source/assets/img/jnktn_logo_blackonyellow_border.png" alt="Jnktn.tv Logo" width="100"/>

[![status-badge](https://gitlab.com//jnktn/jnktn.tv/badges/mistress/pipeline.svg)](https://gitlab.com/jnktn/jnktn.tv/-/pipelines?page=1&scope=all&ref=mistress)

Welcome to the [Jnktn.tv](https://jnktn.tv) repository. Here you'll find the code for our website.

Jnktn.tv website pages are generated using [Hexo](https://hexo.io/). The streaming is powered by [Owncast](https://github.com/owncast/owncast).

## Quick start

### For authors

- Create a new branch (or fork the repository) based on the `mistress` branch.
- Edit the source Markdown files under `source/` and commit your changes.
- After pushing your changes, they will be [built automatically on CI](https://gitlab.com/jnktn/jnktn.tv/-/pipelines). Check the tests and fix any failures. Website pages can be downloaded and examined from the artifacts of the `build-website` job.
- Open a [pull request](https://codeberg.org/jnktn/jnktn.tv/pulls) to merge your changes back into the `mistress` branch.

### For maintainers

- Inspect the changes on your machine, or push the changes to the `develop` branch and check them on [GitLab Pages](https://jnktn.gitlab.io/jnktn.tv/).
- Merge changes into the `mistress` branch to deploy on https://jnktn.tv website.
- If you have pushed to the `develop` branch, delete that branch.

## Developers guide

Details about using Hexo to generate website pages, modifying website content (adding new pages, updating schedule, etc.) modifying the website theme, and the automatic build and deployment process are documented [here](docs/site.md).

## License

© 2020-2024 Jnktn.tv distributed under the terms of the [AGPL-3.0+ license](LICENSE).

See the [Git commits history](https://codeberg.org/jnktn/jnktn.tv/commits/branch/mistress) for the complete list of collaborators.
