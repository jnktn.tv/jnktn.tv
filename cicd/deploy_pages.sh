#!/usr/bin/env bash

# this script deploys the contents of $WWW_ROOT to codeberg pages ($DEPLOY_REPO) at $DEPLOY_BRANCH
# only 10 latest commits are kept
#
# input env variables:
#
# WWW_ROOT
# DEPLOY_BRANCH
# DEPLOY_REPO
# PAGES_PASS

if [ -z "$DEPLOY_BRANCH" ]; then
    if [ -n "$CI_COMMIT_BRANCH" ]; then
        DEPLOY_BRANCH="$CI_COMMIT_BRANCH"
    else
        DEPLOY_BRANCH='main'
    fi
fi

if [ -z "$WWW_ROOT" ]; then
    if [ -n "$CI_WORKSPACE" ]; then
        WWW_ROOT="$CI_WORKSPACE/public"
    else
        WWW_ROOT='public'
    fi
fi

if [ -z "$DEPLOY_REPO" ]; then DEPLOY_REPO='codeberg.org/jnktn/pages.git'; fi;
if [ -z "$CI_COMMIT_AUTHOR" ]; then CI_COMMIT_AUTHOR='unknown'; fi;

# required input variables with no default value
required_env_variables=( "PAGES_PASS" )

for variable in "${required_env_variables[@]}"; do
    if [[ -z ${!variable} ]]; then
        echo "ERROR: \$${variable} is not set!"
        env_err=1
    fi
done

if [[ -n $env_err ]]; then
    echo "Please set required environment variables!"
    exit 1;
fi

echo "website will be pushed to $DEPLOY_REPO and deployed at https://jnktn.codeberg.page/@$DEPLOY_BRANCH/"
set -x

# clone the target branch if available; otherwise, just clone the main branch
git clone --verbose --single-branch --branch="$DEPLOY_BRANCH" --depth=9 https://"${CI_COMMIT_AUTHOR}":"${PAGES_PASS}"@"${DEPLOY_REPO}" || git clone --verbose --depth=1 https://"${CI_COMMIT_AUTHOR}":"${PAGES_PASS}"@"${DEPLOY_REPO}"

cd pages || exit 1;
git config user.email "${CI_COMMIT_AUTHOR}@noreply.codeberg.org" && git config user.name "${CI_COMMIT_AUTHOR}-bot"

# if the branch already exists, cleanup the history and rm all files; else: switch orphan
( git switch "$DEPLOY_BRANCH" && GIT_BASE=$(git rev-list --max-parents=0 HEAD) &&\
git checkout --orphan "${DEPLOY_BRANCH}temp" "$GIT_BASE" &&\
git commit -m "new base" &&\
git rebase --onto "${DEPLOY_BRANCH}temp" "$GIT_BASE" "$DEPLOY_BRANCH" &&\
git branch -D "${DEPLOY_BRANCH}temp" && git rm -r . ) ||\
git switch --orphan "$DEPLOY_BRANCH"

# cleanup git
git reflog expire --expire=all --all && git prune --progress && git gc --aggressive

cp -r -v "${WWW_ROOT}"/* ./ || exit 1;

git add .
git commit -m "deploying $DEPLOY_BRANCH" -m "git source $CI_COMMIT_SHA" --allow-empty
git --no-pager log
git push -f --set-upstream origin "$DEPLOY_BRANCH"
