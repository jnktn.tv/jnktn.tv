#!/usr/bin/env sh
set -e

if [ -z "$CONTEXT" ]; then CONTEXT='.'; fi;

if [ -z "$DEPLOY_ROOT" ]; then DEPLOY_ROOT="/"; fi;

if [ -z "$CONTRIBUTORS_LIST" ]; then CONTRIBUTORS_LIST=$(tr '\n' ' ' < "${CONTEXT}/CONTRIBUTORS"); fi;

identify_commit_hash()
{
    if [ -n "$CI_COMMIT_SHA" ]; then
       echo "$CI_COMMIT_SHA" | awk 'NR==1{printf $NF}'
    else
        git rev-parse HEAD | awk 'NR==1{printf $NF}'
    fi;

}

identify_commit_branch()
{
    if [ -n "$CI_COMMIT_BRANCH" ]; then
        echo "$CI_COMMIT_BRANCH" | awk 'NR==1{printf $NF}'
    else
        git rev-parse --abbrev-ref HEAD | awk 'NR==1{printf $NF}'
    fi;

}

identify_repo_name()
{
    if [ -n "$CI_REPO_NAME" ]; then
        echo "$CI_REPO_NAME" | awk 'NR==1{printf $NF}'
    elif [ -n "$CI_EXTERNAL_PULL_REQUEST_SOURCE_REPOSITORY" ]; then
        echo "$CI_EXTERNAL_PULL_REQUEST_SOURCE_REPOSITORY" | awk 'NR==1{printf $NF}'
    else
        echo "jnktn.tv" | awk 'NR==1{printf $NF}'
    fi;
}

identify_repo_owner()
{
    if [ -n "$CI_REPO_OWNER" ]; then
        echo "$CI_REPO_OWNER" | awk 'NR==1{printf $NF}'
    elif [ -n "$CI_PROJECT_NAMESPACE" ]; then
        echo "$CI_PROJECT_NAMESPACE" | awk 'NR==1{printf $NF}'
    else
        echo "unknown" | awk 'NR==1{printf $NF}'
    fi;
}

# Collect environment variables
commit_hash=$(identify_commit_hash)
commit_branch=$(identify_commit_branch)
repo_name=$(identify_repo_name)
repo_owner=$(identify_repo_owner)
repo_owner_name="$repo_owner/$repo_name"

echo "================================================================================"
echo "Commit Hash      :   $commit_hash"
echo "Commit Branch    :   $commit_branch"
echo "Repository Name  :   $repo_name"
echo "Repository Owner :   $repo_owner"
echo "Repository Slug  :   $repo_owner_name"
echo "Deployment Root  :   $DEPLOY_ROOT"
echo "Contributors     :   $CONTRIBUTORS_LIST"
echo "================================================================================"

printf "hash: %s\nuser_repo: %s" "$commit_hash" "$repo_owner_name" > source/_data/env.yaml
sed -i "s|root: .*|root: $DEPLOY_ROOT|" _config.yml
sed -i "s|url: .*|url: https://jnktn.tv$DEPLOY_ROOT|" _config.yml

npx hexo generate --debug

# just in case! :P
find public -name "*.js" -delete

# cleanup
find public -type f -empty -delete

# minify
find public -name "*.html" -exec sed -i -e 's/^[ \t]*//' {} \;
find public -name "*.html" -exec sed -i -e '/^$/d' {} \;

cd public && ln -s offline.html index.html
