---
title: worky in the Buff
streamer_id: worky
show_id: workyinthebuff
---

<!-- more -->

## Worky in the Buff

[Seems like our team members are too busy to write a few sentences for this page. We’ve recommended them *Successful Time Management: How to be Organized, Productive and Get Things Done* by Patrick Forsyth which may be useful for you too!]

### Hosted by: {% link_for '/team/worky' Worky %}
