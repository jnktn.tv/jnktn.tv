---
title: Get Ready with Andrina
streamer_id: andrina
show_id: getreadywithandrina
---

Grab your make up, dancing shoes and Prosecco as you get ready for your
night in or out as Andrina takes you through skin care and make up routines
whilst jamming to amazing beats.

<!-- more -->

## Get Ready with Andrina

### Hosted by: {% link_for '/team/andrina' Andrina %}

![Get ready with Andrina](../assets/img/shows/getreadywithandrina.png)

Grab your make up, dancing shoes and Prosecco as you get ready for your night in or out as Andrina takes you through skin care and make up routines whilst jamming to amazing beats.

### What's being played?

- 80s/90s Dance
- House & Electro
- Hip Hop
- Rock
- Pop
