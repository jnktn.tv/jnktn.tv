---
title: Dance Attack with Ruffy
streamer_id: ruffy
show_id: danceattackwithruffy
---

Have you ever wondered whether there's a upper limit for the tempo of a song? No? Ruffy neither.

<!-- more -->

## Dance Attack with Ruffy

### Hosted by: {% link_for '/team/ruffy' Ruffy %}

Let me tell you the story of ~DJ Fluffy~ I mean ~DJ Ruffy~ ... Well, doesn't really mather -  a DJ. The legends says, that one night he played pop music at a club, and at some point he noticed that nobody was dancing anymore. People were bound to a curse some ancient cultures called "Smartphones". "That has to change, I need to save them", he said to himself. Next evening he started to play music from the mysterious land of `EDM` that was known to heal all kinds of diseases. And to his surprise it worked - suddenly the crowd noticed a previously unfelt urge to move their feet to the driving beats. Slowly the curse was lifted, people laid down their phones, and as a side-effect also started to loose weight.

Mh, yeah. That story sucks. Dance Attack doesn't! Just tune in, you'll love it!

### The Dance Attack recipe

- take a porition of spiced-up pop songs
- mash them up with decent house & electro
- add a pinch of good ol' hands up music
- transition over to harder hardstyle
- transition over to faster hardcore
- and we've somehow ended up at 250 BPM
