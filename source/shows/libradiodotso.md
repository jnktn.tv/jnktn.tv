---
title: libradio.so
streamer_id: shrik3
show_id: libradiodotso
---

METAL & WAVES by SHRIK3

<!-- more -->

```text
██╗     ██╗██████╗ ██████╗  █████╗ ██████╗ ██╗ ██████╗    ███████╗ ██████╗
██║     ██║██╔══██╗██╔══██╗██╔══██╗██╔══██╗██║██╔═══██╗   ██╔════╝██╔═══██╗
██║     ██║██████╔╝██████╔╝███████║██║  ██║██║██║   ██║   ███████╗██║   ██║
██║     ██║██╔══██╗██╔══██╗██╔══██║██║  ██║██║██║   ██║   ╚════██║██║   ██║
███████╗██║██████╔╝██║  ██║██║  ██║██████╔╝██║╚██████╔╝██╗███████║╚██████╔╝
╚══════╝╚═╝╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝ ╚═╝ ╚═════╝ ╚═╝╚══════╝ ╚═════╝
libradio.so(1)

NAME libreradio - waves and metal daemon
SYNOPSIS
    radio [OPTION] ... [FILE] ...
DESCRIPTION
    -s  Summon satan, specify your tribute
    -b  Bring me the beer
    -d  Tantz Tantz Tantz
    -f  Have fun!

METAL & WAVES by SHRIK3
```

HOSTED BY {% link_for '/team/shrik3' SHRIK3 %}
