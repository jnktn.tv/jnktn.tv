---
title: Vera's Radio Show
streamer_id: vera
show_id: verasradioshow
---

 #ShakeYourBumcheeks with tunes from every genre. No show will be the same!

<!-- more -->

## Vera's Radio Show

### Hosted by: {% link_for '/team/vera' Vera %}

Join Vera through the weeks as she encourages you to #ShakeYourBumcheeks with tunes from every genre. No show is the same, but each week she will make sure to keep viewers and listeners alike engaged with information about the artists as well as random facts.
