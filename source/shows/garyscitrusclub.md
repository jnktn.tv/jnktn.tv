---
title: Gary's Citrus Club
streamer_id: gary
show_id: garyscitrusclub
---

Revisit Edinburgh's Famous Citrus Club for a Saturday Night of Indie and Classic Rock Vibes. Grab your Red Stripe and Peach Schnapps and join your host, The King of the Edinburgh Streets, The 2006 Rhodes Pub Quiz Champion, he's 5 foot 10 with a beard of steel, he's the beast, the legend.....

<!-- more -->

## Gary's Citrus Club

### Hosted by: {% link_for '/team/gary' Gary %}

Revisit Edinburgh's Famous Citrus Club for a Saturday Night of Indie and Classic Rock Vibes. Grab your Red Stripe and Peach Schnapps and join your host, The King of the Edinburgh Streets, The 2006 Rhodes Pub Quiz Champion, he's 5 foot 10 with a beard of steel, he's the beast, the legend.....

### Fun Facts

- The Citrus Club was a club in Edinburgh
- Gary is not only a beast, he is also a legend
