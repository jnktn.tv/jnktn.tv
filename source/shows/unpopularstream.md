---
title: Unpopular Stream
streamer_id: michan
show_id: unpopularstream
---

A weird journey through the sonic landscape! Unpopular Stream is an hour-long playlist, mostly from non-mainstream artists and includes a mixture of electronic music genres from splittercore to dark ambient. It is a walk back road for nights when I'm lost.

<!-- more -->

## Unpopular Stream

### Hosted by: {% link_for '/team/michan' Michan %}

Have you ever thought: what is music and what is not? where are all these genres coming from? Or, what if audience's financial support was not a determining factor in an artist's decisions? Is art only definable by the external validation?

To wonder about this kind of questions and listen to a dialogue between various genres, join us in our next adventure of Unpopular Stream. Many of them are mixed on headphones so bring good headphones for the best experience! The show is generally intended for mature audience.

### Origins

Unpopular Stream is a tribute to **Øfdream**. An artist who inspired a generation.

> “May the legacy of Øfdream live forever.”

{% soundcloud playlist 940150522 %}

### Conclusion

[*to be determined*]
