---
title: Privacy Guide
---

## Privacy Guide

This page gives you basic advise on how to protect your privacy online, and an overview of which steps we’ve taken to ensure the privacy of our users.

You can read our privacy policy [here](./privacy_policy).

### Recommendations

Whenever your are exploring the world ~~wide~~ wild web, chances are, that shady companies are tracking what you do online. As privacy is an important matter at Jnktn, we would like to brief you with some best-practices and tools to help you to protect your privacy online and while surfing on Jnktn.

We don't want to give you a full guide on online-privacy here. But if you're interested, make sure to visit [privacytools.io](https://www.privacytools.io/) for guides to improve your online privacy.

#### Pick the right browser

We recommend browsers listed in [the privacytools.io browser section](https://www.privacytools.io/#browser).

- Mozilla Firefox (Desktop, Mobile)
- Tor Browser (Linux)
- Brave (Desktop, Mobile)
- DuckDuckGo (Mobile)

#### Harden your browser with Addons

We recommend to extend your browser with Addons enhancing its privacy. Jnktn recommends extensions and addons listed in [the privacytools.io addon section](https://www.privacytools.io/#browser-addons).

- uBlock Origin (Adblock)
- HTTPS Everywhere (Redirects to HTTPS)
- Decentraleyes (Local CDN Cache)
- ClearURLs (clears tracking Parameters from URLs)

#### Best Practices to protect your Privacy on Jnktn

- Whenever possible, use a `Private Window` of your browser.
  [jnktn.tv](https://jnktn.tv) can be visited in a private window flawlessly,
- Follow [Jnktn.TV's Mastodon Account](https://mastodon.online/@jnktn_tv)
  - instead of [Instagram](https://www.instagram.com/jnktn.tv/)
  - and instead of [Twitter](https://twitter.com/jnktn_tv)
- If you still want to see what's happening on Instagram and Twitter, use
  - [bibliogram](https://git.sr.ht/~cadence/bibliogram) as privacy-friendly frontend for Instagram
  - [nitter](https://github.com/zedeus/nitter) as privacy-friendly frontend for Twitter
- If you don't want to chat with the lovely people at Jnktn and just want to watch the stream
  - open up the `https://stream.jnktn.tv` URL in your media player (e.g. VLC/mpv)

### Measures Jnktn.TV takes to protect your privacy

- Jnktn.TV does not utilize ANY site-analytics application
- Jnktn.TV (except Owncast) does not ship ANY JavaScript to its visitors
  - no JavaScript will be executed on the client-side
- Jnktn.TV self-hosts resources like Images, Stylesheets and Fonts
  - this prevents third-parties such as CDNs to spy on your visit at Jnktn
- Jnktn.TV webserver only uses modern TLS protocols with secure ECDH cipher suites
- Jnktn.TV enforces a strong set of Security-Headers
  - including a CSP preventing XSS and loading resources from other domains
  - a referrer policy, preventing linked websites to know where you came from
- DNSSEC is enabled for the Jnktn.TV and the domain is also included in the HTTP HSTS preload list of all major browsers
