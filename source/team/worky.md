---
streamer: Worky
streamer_id: worky
title: Worky
tags:
    - Streamer
    - DJ
---

Nice Shirts, doesn't stay still stylistically and frequently makes the whole dancefloor climax. Also one of the Jnktn Co-creators.

<!-- more -->

## Worky

### Intro

Having burst onto the Glasgow scene in 2009 worky quickly made a name for himself playing at various events around the city & getting offered sets at world class venues such as "The Arches & Sub Club" which led to numerous residencies and supporting acts as diverse as Matthew Dear, Alexis Taylor, Booka Shade, Nightmares on Wax, Unkle, DJ EZ, Cyril Hahn, Easy Life, Homeshake, Beardyman & Lone just to name a few.

<img src="../assets/img/team/worky.jpg" height="400" width="400" style="margin: 10px auto; display: block;" alt="worky"/>

worky is known for playing 4 or 5 hour long sets where he doesn't stay still stylistically but with a tune selection & technical ability that is up with some of the best in the game! He's equally at home in a foggy warehouse, dirty basement or at a summer festival and his sets are always filled with energy, surprise twists & turns to keep you on your toes & I guarantee he's going to make you dance!

### Fact sheet

| Facts          |              |
|:--------------:|:------------:|
| Country        | Scotland 🏴󠁧󠁢󠁳󠁣󠁴󠁿  |
| Pronouns       | he/him       |
| Joined Jnktn   | 2020         |

#### Resident DJ

- [@thebuffclub](https://linktr.ee/thebuffclub)
- [@rollerstop](https://www.rollerstop.co.uk)
- [@thelocaleglasgow](https://www.thelocaleglasgow.com)

### More of worky

{% mixcloud https://www.mixcloud.com/worky/the-church-of-good-times-w-mrrrkk-ali-tucker-worky/ %}

{% mixcloud https://www.mixcloud.com/worky/worky-got-stoned-and-mixed-tunes-for-2-hours/ %}

### Get in touch

- [Instagram](https://www.instagram.com/worky_gla/)
- [Mixcloud](https://www.mixcloud.com/worky)
