---
streamer: Gary
streamer_id: gary
title: Gary
picture: none
country: 🏴󠁧󠁢󠁳󠁣󠁴󠁿
tags:
    - Streamer
---

Some call him the beast. Others call him the legendary. We call him both.
No matter how you name him, he's a cool rock-head.

<!-- more -->

## Gary

[Seems like our team members are too busy to write a few sentences for this page. We’ve recommended them *Successful Time Management: How to be Organized, Productive and Get Things Done* by Patrick Forsyth which may be useful for you too!]
