---
streamer: Andrina
streamer_id: andrina
title: Andrina
picture: none
country: 🏴󠁧󠁢󠁳󠁣󠁴󠁿
tags:
    - Streamer
---

Getting ready already?

<!-- more -->

## Andrina

| Facts          |              |
|:--------------:|:------------:|
| Country        | Scotland 🏴󠁧󠁢󠁳󠁣󠁴󠁿  |
| Pronouns       | she/her      |
| Joined Jnktn   | 2020         |
| Dogs Vs. Cats  | ![catjam](../assets/img/memes/catjam.gif) |
| Advice for you | If it makes you feel good - do it! |

### About

When Andrina is not busy getting ready for the evening, she enjoys reading a good book or dancing through the night to her favorite tunes. Be it 90s dance tunes, R&B and Hip-Hop or oldschool disco tracks. Andrina for sure is in. Her taste in music was influenced by 2 of the biggest names in the world of electronic dance music.

- [Don Diablo](https://www.dondiablo.com)
- [Benny Benassi](https://www.bennybenassi.com/)

### Favorite genres

- Classic 90s dance
- old school R&B/hip hop
- disco

### Best Jnktn Moments

- Jnktn does Eurovision
- Jnktn Christmas bonanza
