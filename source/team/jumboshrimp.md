---
streamer: Jumboshrimp
streamer_id: jumboshrimp
title: Jumboshrimp
#picture: https://www.jnktn.tv/assets/img/team/jumboshrimp.png
country: 🏴󠁧󠁢󠁳󠁣󠁴󠁿
tags:
    - Project Lead
    - Streamer
    - Tech
---

A hot mess at the best of times. I'm not afraid to get sand on my tuxedo if you're not afraid to let the wind mess your hair up a little bit when I take the top down.

<!-- more -->

## Jumboshrimp

![jumboshrimp](../assets/img/team/jumboshrimp.png)

| Facts        |              |
|:------------:|:------------:|
| Country      | Scotland 🏴󠁧󠁢󠁳󠁣󠁴󠁿  |
| Pronouns     | he/him       |
| Joined Jnktn | 2020         |

### Intro

A hot mess at the best of times. I'm not afraid to get sand on my tuxedo if you're not afraid to let the wind mess your hair up a little bit when I take the top down.

## Interests

Interests include ...

- open source software
- advocating for privacy
- music of all sorts
- vidya games
- traveling
- campaigning for the 0 day workweek

### Get in touch

- [Mastodon](https://fosstodon.org/@jumboshrimp)
