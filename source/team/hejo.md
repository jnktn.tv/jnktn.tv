---
streamer: Hejo
streamer_id: hejo
title: Hejo
tags:
    - Graphics
    - Tech
---

<!-- more -->

## Hejo

| Facts        |           |
|:------------:|:---------:|
| Joined Jnktn | 2021      |

### Who?

Hi there! I am Hejo, and I help from time to time. Creating graphics or giving mediocre tech advice.

### Why am I here?

Great people, good music.

### Stranded on an island

What 3 songs would I save?

- Gary Jules - Mad World
- Slipknot - Duality
- Black Eyed Peas - Someday

Pretty generic, but these songs will keep me alive.

When I am not stranded on an island, I usually listen to all kinds of electronic music, American rap, rock, and more.
