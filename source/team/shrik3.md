---
streamer: Shrik3
streamer_id: shrik3
title: Shrik3
tags:
    - Streamer
---

ACHTUNG: THIS MUSIC SHARING BOT IS BECOMING SELF-CONSCIOUS!!!

<!-- more -->

```text
  __     _____ _    _ _____  _____ _  ______     __
  ||    / ____| |  | |  __ \|_   _| |/ /___ \    ||
  ||   | (___ | |__| | |__) | | | | ' /  __) |   ||
__||__  \___ \|  __  |  _  /  | | |  <  |__ <  __||__
--..--  ____) | |  | | | \ \ _| |_| . \ ___) | --..--
  ||   |_____/|_|  |_|_|  \_\_____|_|\_\____/    ||
```

## define

ACHTUNG: THIS MUSIC SHARING BOT IS BECOMING SELF-CONSCIOUS!!!

---

> Shrikes (/ʃraɪk/) are carnivorous passerine birds of the family Laniidae. The family name, and that of the largest genus, Lanius, is derived from the Latin word for “butcher”, and some shrikes are also known as butcherbirds because of their feeding habits.

## generates

THIS ONE IS ALSO TRYING TO REPRODUCE

- [Vom Verborgenen](https://vomverborgenen.bandcamp.com/album/vom-verborgenen): METAL
- [Normandy 2183](https://normandy2183.bandcamp.com/) OUTRUN
- [die Todgeweihten](https://shrik3.bandcamp.com/) INDUSTRIAL/EBM

## pointers

- [FEDI](https://vnil.de/shrik3)
- [BLOG](https://shrik3.com)
- MATRIX - @shrik3:matrix.vnil.de

## scope

METAL, WAVES, OUTRUN, INDUSTRIAL
