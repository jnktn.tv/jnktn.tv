---
streamer: Vera
streamer_id: vera
title: Vera
picture: none
country: 🏴󠁧󠁢󠁳󠁣󠁴󠁿
tags:
    - Streamer
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Placerat duis ultricies lacus sed turpis tincidunt id aliquet risus.

<!-- more -->

## Vera

| Facts        |              |
|:------------:|:------------:|
| Country      | Scotland 🏴󠁧󠁢󠁳󠁣󠁴󠁿  |
| Pronouns     | she/her      |
| Joined Jnktn | 2020         |

### Intro

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Placerat duis ultricies lacus sed turpis tincidunt id aliquet risus. Sed odio morbi quis commodo odio aenean. Egestas sed tempus urna et pharetra pharetra massa massa ultricies. Risus quis varius quam quisque id. A diam sollicitudin tempor id eu nisl nunc mi ipsum. Sed blandit libero volutpat sed cras ornare arcu dui vivamus. Senectus et netus et malesuada fames ac turpis egestas integer. Dui accumsan sit amet nulla facilisi. Est velit egestas dui id ornare. Erat imperdiet sed euismod nisi porta lorem mollis aliquam ut. Semper quis lectus nulla at volutpat diam. Sit amet commodo nulla facilisi. Feugiat in fermentum posuere urna. Id aliquet lectus proin nibh nisl condimentum id venenatis. Sem viverra aliquet eget sit amet tellus cras.
