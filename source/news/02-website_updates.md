---
title: New website!
author: michan
date: 2024-10-26
excerpt: "Look at our shiny new website!"
---

If you are reading this message, then our new website is already online. This website is statically generated using [hexo](https://hexo.io). We have added a dedicated section for {% link_for '/team' members %}, {% link_for '/shows' 'regular shows' %}, and {% link_for '/news' news %}. The streaming schedule is generated from a yaml file to prepare for integration with a dedicated stream scheduling app. Most of the coding has been done by our great {% link_for '/team/ruffy' Ruffy %}!

The website pages are automatically generated on GitLab CI, tested for dead links and deployed to the webserver.
